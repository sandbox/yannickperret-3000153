// manage tabs
// evt: the event. name: the name of the selected tab.
// id: identifier for the group of tabs
function switch_tab(evt, name, id) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // get all elements with class="tabcontent-id" and hide them
    tabcontent = document.getElementsByClassName("tabcontent-" + id);
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks-id" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks-" + id);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(name).style.display = "block";
    evt.currentTarget.className += " active";
}

