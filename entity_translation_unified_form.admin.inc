<?php


/**
 * Returns true if given content type is handled by Entity translation
 */
function _entity_translation_unified_form_is_node_translatable($type) {
  $entity_info = entity_get_info('node');
  if (isset($entity_info['translation']['entity_translation']['bundle callback'])) {
    $callback = $entity_info['translation']['entity_translation']['bundle callback'];
  } else {
    $callback = "";
  }
  return empty($callback) || call_user_func($callback, $type);
}

/**
 * Create admin settings
 */
function entity_translation_unified_form_admin_settings($form, &$form_state) {
  $form = [];
  
  // description
  $form['label'] = array(
    '#markup' => '<h2>Entity translation unified form settings page.</h2>',
  );
  
  // fields title modification
  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['fields']['fields_label'] = array(
    '#markup' => '<p>Configurations related to fields.</p>',
  );
  // tabs text
  $form['fields']['fields_title'] = array(
    '#type' => 'select',
    '#title' => t('Text added to fields'),
    '#default_value' => variable_get('entity_translation_unified_form_fields_title', 0),
    '#options' => array(t('Use language name'), t('Use language code')),
  );
  
  // Tab-related configuration
  $form['tabs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tabs configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['tabs']['tabs_label'] = array(
    '#markup' => '<p>Configurations related to tabs presentation.</p>',
  );
  
  // switch beetween no-tabs/tabs
  $form['tabs']['tabs_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable tabs'),
    '#default_value' => variable_get('entity_translation_unified_form_tabs_enable', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  
  // tabs text
  $form['tabs']['tabs_title'] = array(
    '#type' => 'select',
    '#title' => t('Tabs text'),
    '#default_value' => variable_get('entity_translation_unified_form_tabs_title', 0),
    '#options' => array(t('Use language code'), t('Use language name')),
  );

  // content-types
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content-types'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['types']['types_label'] = array(
    '#markup' => '<p>Configurations related to content-types.</p>',
  );
  $form['types']['types_label2'] = array(
    '#markup' => '<p>Content-types available for entity-translation unified form:</p>',
  );
  // for each content-type
  $types = node_type_get_types();
  $i = 0;
  $other = [];
  foreach($types as $mt => $_t) {
    $t = $_t->type;
    if (_entity_translation_unified_form_is_node_translatable($mt)) {
      $form['types']["types_translatable_$t"] = array(
        '#type' => 'checkbox',
        '#title' => "<b>$t</b>",
        '#default_value' => variable_get('entity_translation_unified_form_enable_' . $t, FALSE),
        '#options' => array(t('Disabled'), t('Enabled')),
      );
    } else {
      $other[] = $mt;
    }
    $i++;
  }
  // non translatable (for info)
  $form['types']['types_label3'] = array(
    '#markup' => '<p>For information, content-types not available for entity-translation unified form:</p>',
  );
  $form['types']['types_label4'] = array(
    '#markup' => '<p>' . implode(" ; ", $other) . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  
  return $form;
}

/**
 * Admin form submit function
 */
function entity_translation_unified_form_admin_settings_submit($form, &$form_state) {
  $value = $form_state['values'];
  
  // tabs enabled?
  if (isset($value["tabs_enable"])) {
    $tabs_enable = $value["tabs_enable"];
  } else {
    $tabs_enable = 0;
  }
  variable_set('entity_translation_unified_form_tabs_enable', $tabs_enable);
  // tabs title
  if (isset($value["tabs_title"])) {
    $tabs_title = $value["tabs_title"];
  } else {
    $tabs_title = 0;
  }
  variable_set('entity_translation_unified_form_tabs_title', $tabs_title);
  // fields title
  if (isset($value["fields_title"])) {
    $fields_title = $value["fields_title"];
  } else {
    $fields_title = 0;
  }
  variable_set('entity_translation_unified_form_fields_title', $fields_title);
  // content-types enabled
  $types = node_type_get_types();
  foreach($types as $mt => $_t) {
    $t = $_t->type;
    if (_entity_translation_unified_form_is_node_translatable($mt)) {
      if (isset($value["types_translatable_$t"])) {
        $val = $value["types_translatable_$t"];
      } else {
        $val = FALSE;
      }
      $cur = variable_get('entity_translation_unified_form_enable_' . $t, FALSE);
      // if selected value is different than current config, change it
      if ($val != $cur) {
        variable_set('entity_translation_unified_form_enable_' . $t, $val);
      }
    }
  }
      
  // message to admin
  drupal_set_message(t('Settings saved.'));
}

/* ?> */

