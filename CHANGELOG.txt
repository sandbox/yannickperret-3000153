Changes from 7.x-2.0-beta1:
* added admin settings form
  - path admin/config/regional/entity_translation_unified
  - available from menu Configuration > Regionalization and languages > Entity translation unified form
  - available from 'configure' in modules list
  - setting for enabling/disabling (default) tabs
  - setting for changing text added to fields title (language name (default) / language code)
  - setting for changing tabs title (language code (default) / language name)
  - settings for enabling/disabling (default) ETUF for each available content-types
* changed fields title modification to handle corresponding setting
* added tabs wrapper around translatable fields
  - tabs title depend of setting
  - inline JS included in order to pre-select default language
* added CSS to module
  - handle theming of tabs buttons and tabs elements
* added JS to module
  - handle action when clicking on tabs
* added validation function to handle red-hilighting tabs titles corresponding to fields in error

